from web.models import db
from web.models.user import User


def register(username: str, email: str, password: str):
    """Register new user

    :arg username: username user
    :arg email: email user
    :arg password: password user
    """
    user = User(
        username=username,
        email=email,
        password=password
    )
    db.session.add(user)
    db.session.flush()
