from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from web.views import auth, analytic
from web.models import db
from web.models.user import User
from web.models.product import Product
from web.models.ecommerce import Ecommerce
from web.login_manager import login_manager
from web.bcrypt import bcrypt
from web.migrate import migrate
from configuration import Config


def create_app():
    app = Flask(__name__)

    app.config.from_object(Config)

    # blueprint
    app.register_blueprint(auth.bp)
    app.register_blueprint(analytic.bp)

    # plugins initialization
    db.init_app(app)
    login_manager.init_app(app)
    bcrypt.init_app(app)
    migrate.init_app(app)

    # administrator view
    admin = Admin(app, name='eanalytic', template_mode='bootstrap3')
    admin.add_view(ModelView(User, db.session))
    admin.add_view(ModelView(Product, db.session))
    admin.add_view(ModelView(Ecommerce, db.session))

    return app
