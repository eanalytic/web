from web.models import db


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    image_path = db.Column(db.String(80))

    price = db.Column(db.Integer)

    title = db.Column(db.String(80))

    url = db.Column(db.String(120))

    ecommerce_id = db.Column(db.Integer, db.ForeignKey('ecommerce.id'))

    ecommerce = db.relationship('Ecommerce', backref='product')
