from web.models import db


class Ecommerce(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(50))

    def __repr__(self):
        return self.name
