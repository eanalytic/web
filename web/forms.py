import wtforms


class RegistrationForm(wtforms.Form):
    username = wtforms.StringField(
        'Username', [wtforms.validators.Length(min=4, max=25)]
    )

    email = wtforms.StringField(
        'Email Address', [wtforms.validators.Length(min=6, max=35)]
    )

    password = wtforms.PasswordField(
        'New Password', [
            wtforms.validators.DataRequired(),
            wtforms.validators.EqualTo('confirm', message='Passwords must match')
        ])

    confirm = wtforms.PasswordField('Repeat Password')

    accept_tos = wtforms.BooleanField(
        'I accept the TOS', [wtforms.validators.DataRequired()]
    )
