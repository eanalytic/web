from flask import Blueprint, render_template, request

bp = Blueprint("analytic", __name__)


@bp.route("/")
def search():
    """Search market"""
    return render_template("analytics/search.html")


@bp.route("/analytic")
def analytic():
    """Show market analytics"""
    keyword = request.args.get('keyword')
    return render_template("analytics/analytic.html", keyword=keyword)
