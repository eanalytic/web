from flask import Blueprint, render_template, request, redirect
from web.forms import RegistrationForm
from web import user

bp = Blueprint("auth", __name__)


@bp.route("/login")
def login():
    return render_template("auth/login.html")


@bp.route("/register")
def register():
    form = RegistrationForm()
    if request.method == "POST" and form.validate():
        user.register(
            username=form.username.data,
            email=form.email.data,
            password=form.password.data
        )
        return redirect("/login")

    return render_template("auth/register.html", form=form)


@bp.route("/recover-password")
def forgot():
    return render_template("auth/recover-password.html")
